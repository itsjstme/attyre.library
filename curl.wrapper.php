<?php

error_reporting(E_ERROR | E_PARSE);

/**
* Agile CRM \ Curl Wrap
*
* The Curl Wrap is the entry point to all services and actions.
*
* @author    Agile CRM developers <Ghanshyam>
*/

# Enter your domain name , agile email and agile api key
define("AGILE_DOMAIN",  getenv('AGILE_DOMAIN')  ); # Example : define("domain","jim");
define("AGILE_USER_EMAIL",  getenv('AGILE_USER_EMAIL') );
define("AGILE_REST_API_KEY",  getenv('AGILE_REST_API_KEY')  );

$servername = "127.0.0.1";
$database = "attylvke_listener";
$username = "attylvke_all";
$password = getenv('DB_PASSWORD')  ;
$charset = "utf8mb4";

$pdo = null;

if ( getenv('HOME')  == "/home/attylvke") {

    try {

        $dsn = "mysql:host=$servername;dbname=$database;charset=$charset";
        $pdo = new PDO($dsn, $username, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch (PDOException $e) {
        $pdo = null;
        trigger_error("Connection failed: " . $e->getMessage(), E_USER_NOTICE);
    }

}

function myErrorHandler($errno, $errstr, $errfile, $errline)
{
    global $pdo;

    $site = "attyredelivery";
    $data = "";
    

    if (empty($pdo)) {

        logger("%file% %errline% %message%", ["message" => $errstr, "errline" => $errline, "file" => $errfile ]);

    } else {
        
        list($message, $data) = explode("|", $errstr);
        $sql = "INSERT INTO `error_log` (`error_number`, `error_message`, `error_file`, `error_line`, `data`, `site`) VALUES (?,?,?,?,?,?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$errno, $message, $errfile, $errline, $data, $site]);

    }

    /* Don't execute PHP internal error handler */
    return true;

}

$old_error_handler = set_error_handler("myErrorHandler");

function curl_wrap($entity, $data, $method, $content_type)
{
    trigger_error("curl_wrap START", E_USER_NOTICE);

    if ($content_type == null) {
        $content_type = "application/json";
    }

    trigger_error("curl_wrap content type: ${content_type}", E_USER_NOTICE);

    $agile_url = "https://" . AGILE_DOMAIN . ".agilecrm.com/dev/api/" . $entity;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_UNRESTRICTED_AUTH, true);
    switch ($method) {
        case "POST":
            $url = $agile_url;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            trigger_error("POST {$url} | ${data}", E_USER_NOTICE);
            break;
        case "GET":
            $url = $agile_url;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            trigger_error("GET {$url}", E_USER_NOTICE);
            break;
        case "PUT":
            $url = $agile_url;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            trigger_error("PUT {$url} | ${data}", E_USER_NOTICE);
            break;
        case "DELETE":
            $url = $agile_url;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            trigger_error("DELETE {$url}", E_USER_NOTICE);
            break;
        default:
            break;
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type:$content_type;", 'Accept:application/json',
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERPWD, AGILE_USER_EMAIL . ':' . AGILE_REST_API_KEY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $output = curl_exec($ch);
    curl_close($ch);

    trigger_error("curl_wrap END | {$output}", E_USER_NOTICE);
    return $output;
}

function logger($message, array $data, $debugMode = false)
{
    $folder = getcwd();

    foreach ($data as $key => $val) {
        $message = str_replace("%{$key}%", $val, $message);
    }

    $message .= PHP_EOL;

    if (defined('DEBUG')) {
        echo $message;
    } else {
        error_log('[' . date("F j, Y, g:i a e O") . ']' . $message, 3, "{$folder}/error_log");

    }

}

function safeEncrypt(string $message): string
{
    // Store the cipher method
    $ciphering = "AES-128-CTR";

    // Use OpenSSl Encryption method
    $iv_length = openssl_cipher_iv_length($ciphering);
    $options = 0;
    // Non-NULL Initialization Vector for encryption
    $encryption_iv = getenv('OPENSSL_VI') ;

    // Store the encryption key
    $encryption_key = getenv('OPENSSL_KEY');

    // Use openssl_encrypt() function to encrypt the data
    return openssl_encrypt($message, $ciphering, $encryption_key, $options, $encryption_iv);

}

function safeDecrypt(string $message): string
{
    $ciphering = "AES-128-CTR";
    $options = 0;
    // Non-NULL Initialization Vector for decryption
    $decryption_iv = getenv('OPENSSL_VI') ;

    // Store the decryption key
    $decryption_key = getenv('OPENSSL_KEY') ;

    // Use openssl_decrypt() function to decrypt the data
    return openssl_decrypt($message, $ciphering, $decryption_key, $options, $decryption_iv);
}

function process_webhook($verified, $data, $debugMode = false)
{

    $return['status'] = false;
    $return['msg'] = '';
    trigger_error("process_webhook START", E_USER_NOTICE);

    $contactId = null;

    if ($verified && $data[shipping] == false) {

        trigger_error("Verified {$verified} --- Data " . json_encode($data), E_USER_NOTICE);

        $string_address = [$data['address1'], $data['address2']];

        $address = array("address" => implode(" ", $string_address), "city" => $data['city'], "state" => $data['state'], "country" => $data['country']);
        $contact_json = array("tags" => array("Customer"), "properties" => array(
            array("name" => "first_name", "value" => $data['firstName'], "type" => "SYSTEM"),
            array("name" => "last_name", "value" => $data['lastName'], "type" => "SYSTEM"),
            array("name" => "email", "value" => $data['email'], "type" => "SYSTEM"),
            array("name" => "address", "value" => json_encode($address), "type" => "SYSTEM"),
            array("name" => "phone", "value" => $data['phone'], "type" => "SYSTEM"),
            array("name" => "company", "value" => $data['vendor'], "type" => "SYSTEM"),

        ));

        ### check if user exits
        trigger_error("Email search: {$data[email]}", E_USER_NOTICE);
        $contact = json_decode(curl_wrap("contacts/search/email/$data[email]", null, "GET", null));
        $contact_json_input = "";

        trigger_error("Contact Information | " . json_encode($contact), E_USER_NOTICE);

        $contact_message = "";

        // add new contact
        if (empty($contact)) {
            ### creates new contact
            $contact_json_input = json_encode($contact_json);
            $status = json_decode(curl_wrap("contacts", $contact_json_input, "POST", "application/json"));
            $message = "New contact: " . $contact_json_input;
            $contact_message = "New";
            trigger_error("Adding New Contact", E_USER_NOTICE);
            $contactId = $status->id;

        } else {
            $contactId = $contact->id;
        }

        if ($contactId) {

            trigger_error("Contact Information Successfully Update | " . json_encode($status), E_USER_NOTICE);
            $secret_parts = array($contactId, $data[orderNumber]);
            $encrypt = safeEncrypt(implode("|", $secret_parts));

            $contact_json["id"] = $contactId;
            $contact_json["properties"][] = array("name" => "SecretId", "value" => $encrypt, "type" => "CUSTOM");

            $contact_json_input = json_encode($contact_json);
            $status = json_decode(curl_wrap("contacts/edit-properties", $contact_json_input, "PUT", "application/json"));

            $message = "Update contact: " . $contact_json_input;
            $contact_message = "Update";
            trigger_error("Updating Existing Contact", E_USER_NOTICE);

            foreach ((array)$data['products'] as $item) {
                $order[] = array("quantity" => $item->quantity,
                    "productId" => $item->productId,
                    "description" => $item->name,
                    "price" => $item->price);
            }

            $opportunity_json = array(
                "name" => "Order # {$data['orderNumber']} from {$data['vendor']}",
                "expected_value" => 10,
                "milestone" => "Order",
                "description" => json_encode($order),
                "probability" => 100,
                "tagsWithTime" => array($encrypt),
                "contact_ids" => (isset($data['companyId'])) ? array($contactId, $data['companyId']) : array($contactId),
            );

            $deal_json = json_encode($opportunity_json);

            $order = json_decode(curl_wrap("opportunity", $deal_json, "POST", "application/json"));

            if ($order->id) {
                trigger_error("Deal Added | " . json_encode($deal_json), E_USER_NOTICE);
            } else {
                trigger_error("Deal Not Added | " . json_encode($deal_json), E_USER_NOTICE);
            }

            $return['status'] = true;
            $return['msg'] = $contact_message;
            $return['id'] = $contactId;
            $return['deal_id'] = $order->id;

        } else {
            $return[msg] = 'Error creating or updating contact';
            trigger_error($return['msg'], E_USER_NOTICE);
        }

    } else {
        $return['msg'] = 'Webhook not validated ';
        trigger_error("Webhook not validated", E_USER_NOTICE);
    }

    trigger_error("process_webhook END", E_USER_NOTICE);
    return json_encode($return);

}

function verify_jwt_token($token, $secret)
{
    return Token::validate($token, $secret);
}

function verify_webhook($data, $hmac_header, $secret, $debugMode = false)
{
    $calculated_hmac = base64_encode(hash_hmac('sha256', $data, $secret, true));
    trigger_error(" header : ${hmac_header} == calculated:  ${calculated_hmac} == secret: {$secret} ", E_USER_NOTICE);
    return hash_equals($hmac_header, $calculated_hmac);
}
